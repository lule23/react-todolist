import React from "react";
import Todo from "./components/Todo/Todo";

class App extends React.Component {
  state = {
    todos: ["wash car", "clean house", "take a dog for walk"],
    inputValue: "",
  };

  addTodo = (event) => {
    this.setState({ inputValue: "" });

    event.preventDefault();
    const oldTodos = this.state.todos;
    oldTodos.push(this.state.inputValue);

    this.setState({ todos: oldTodos });
  };

  handleChange = (event) => {
    const inputValue = event.target.value;
    // this.setState({ inputValue: inputValue });
    this.setState({ inputValue }); // skracena verzija
  };

  render() {
    return (
      <div>
        <h1>Tasks for today</h1>
        {this.state.todos.map((item, index) => {
          return <Todo key={item} name={item} />;
        })}

        <form onSubmit={this.addTodo}>
          <label htmlFor="addTodo">Todo name</label>
          <input
            id="addTodo"
            type="text"
            value={this.state.inputValue}
            onChange={this.handleChange}
          />
          <button type="submit">Add todo</button>
        </form>
      </div>
    );
  }
}

export default App;
