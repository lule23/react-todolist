import React from "react";

const Counter = (props) => {
  return (
    <div>
      <h3>Counter value: {props.counterValue}</h3>
      <button onClick={props.increment}>Increment</button>
      <button onClick={props.decrement}>Decrement</button>
    </div>
  );
};

export default Counter;
