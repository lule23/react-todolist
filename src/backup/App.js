import React from "react";
import Counter from "./components/Counter/Counter";

class App extends React.Component {
  state = {
    counter: 1,
    counter2: 5,
    name: "Luka",
  };

  incrementCounter = (incrementValue = 5) => {
    this.setState((prevState) => {
      return {
        counter: prevState.counter + incrementValue,
      };
    });
  };

  decrementCounter = () => {
    this.setState((prevState) => {
      return {
        counter: prevState.counter - 1,
      };
    });
  };

  incrementCounter2 = (incrementValue = 5) => {
    this.setState((prevState) => {
      return {
        counter2: prevState.counter2 + incrementValue,
      };
    });
  };

  decrementCounter2 = () => {
    this.setState((prevState) => {
      return {
        counter2: prevState.counter2 - 1,
      };
    });
  };

  componentDidMount() {
    // komponenta se mountovala na ekran
    //  api pozivi ka serveru se definišu ovde
  }

  componentDidUpdate() {
    // poziva se kada se dogodi update komponente, naprimer promeni se state
  }

  render() {
    // ovo je obavezna metoda
    return (
      <div>
        <Counter
          counterValue={this.state.counter}
          increment={() => this.incrementCounter(15)}
          decrement={this.decrementCounter}
        />

        <Counter
          counterValue={this.state.counter2}
          increment={() => this.incrementCounter2(5)}
          decrement={this.decrementCounter2}
        />
      </div>
    );
  }
}

export default App;
